package stocuri.forms;


	import java.util.LinkedList;
import java.util.List;

import Stocuri.model.entities.Angajat;
import Stocuri.model.entities.DocInsotitor;
import Stocuri.model.entities.LinieReturnare;
import Stocuri.model.entities.Partener;
import Stocuri.model.entities.Returnare;
import Stocuri.model.entities.Vanzare;
import Stocuri.model.repository.DocumentRepository;
import Stocuri.model.repository.DocumentRepositoryDefault;
import Stocuri.model.repository.MasterRepository;
import Stocuri.model.repository.MasterRepositoryDefault;

	

	public class ReturnareFormData  {

		// o suita de constante necesare mai tarziu
		public static final String RETURNARE_CU_BON_FISCAL = "Returnare cu bon fiscal";
		public static final String RETURNARE_CU_FACTURA = "Returnare cu factura";
		

		// --------------ZONA 0 - COMUNA TUTUROR FORMULARELOR -------------------//

		// obiectul-tinta (sursa de date) al formularului - cel aflat curent in
		// editare
		// (in cazul nostru poate fi factura sau aviz)

		private DocInsotitor documentCurent;

		// colectia de obiecte-tinta obtinute in urma unei operatii de cautare
		private List<DocInsotitor> listaDocumente;

		// obiecte de tip Repository necesare pentru interogarea modelului
		private MasterRepository masterRepo = new MasterRepositoryDefault();
		private DocumentRepository docRepo = new DocumentRepositoryDefault();

		// si metodele de acces (getteri/setteri) - mutate la finalul clasei pentru
		// claritate

		// ---------------ZONA 1 - PartenerI --------------//
		/**
		 * Pentru incarcarea elementelor din lista (2) cboTipOperatie. Obiectul de
		 * sincronizare selectie corespunzator este documentCurent.Partener
		 */
		private List<Partener> listaPartenerilor;

		/**
		 * implementare standard pentru o colectie-sursa de date a unui obiect de
		 * tip lista
		 * 
		 * @return
		 */
		public List<Partener> getListaPartenerilor() {
			if (this.listaPartenerilor == null) // e primul apel (userul tocmai a
			// deschis formul)
			{
				// this.masterRepo.findParteneriAll();//varianta neoptimizata va
				// dura mai mult si va returna si obiecte Localitate, atasate prin
				// relatia Partener-ManyToOne-Localitate. In cazul nostru nu avem
				// nevoie de obiecte Localitate atasate obiectelor Partener
				this.listaPartenerilor = this.masterRepo.findParteneriAllLight();
			}
			return listaPartenerilor;
		}

		public void setListaPartenerilor(List<Partener> listaPartenerilor) {
			this.listaPartenerilor = listaPartenerilor;
		}

		// Getteri si Setteri pentru manevrarea slectiei curente.
		// lucreaza cu obiectul atasat documentului aflat in editare.
		public Partener getPartenerSelectat() {
			return this.documentCurent.getPartener();
		}

		/**
		 * ATENTIE - in majoritatea mediilor de dezvoltare, obiectele grafice de tip
		 * lista returneaza un id (o valoare atomica / un singul atribut) al
		 * obiectului selectat din lista. Ca urmare, trebuie incarcat obiectul
		 * corespunzator pentru relatia ManyToOne.
		 * 
		 * @param PartenerSelectat
		 */
		public void setPartenerSelectat(Partener PartenerSelectat) {
			Partener PartenerComplet = this.masterRepo
					.findPartenerById(PartenerSelectat.getId());
			this.documentCurent.setPartener(PartenerComplet);
		}
		//------------Angajat---------
		private List<Angajat> listaAngajatilor;
		

		/**
		 * implementare standard pentru o colectie-sursa de date a unui obiect de
		 * tip lista
		 * 
		 * @return
		 */
		public List<Angajat> getListaAngajatilor() {
			if (this.listaAngajatilor == null) // e primul apel (userul tocmai a
			// deschis formul)
			{
				// this.masterRepo.findParteneriAll();//varianta neoptimizata va
				// dura mai mult si va returna si obiecte Localitate, atasate prin
				// relatia Partener-ManyToOne-Localitate. In cazul nostru nu avem
				// nevoie de obiecte Localitate atasate obiectelor Partener
				this.listaAngajatilor = this.masterRepo.findAngajatAllLight();
			}
			return listaAngajatilor;
		}

		public void setListaAngajatilor(List<Angajat> listaAngajatilor) {
			this.listaAngajatilor = listaAngajatilor;
		}

		// Getteri si Setteri pentru manevrarea slectiei curente.
		// lucreaza cu obiectul atasat documentului aflat in editare.
		public Partener getAngajatSelectat() {
			return this.documentCurent.getPartener();
		}

		/**
		 * ATENTIE - in majoritatea mediilor de dezvoltare, obiectele grafice de tip
		 * lista returneaza un id (o valoare atomica / un singul atribut) al
		 * obiectului selectat din lista. Ca urmare, trebuie incarcat obiectul
		 * corespunzator pentru relatia ManyToOne.
		 * 
		 * @param PartenerSelectat
		 */
		public void setAngajatSelectat(Angajat angajatSelectat) {
			Angajat angajatComplet = this.masterRepo
					.findAngajatByID(angajatSelectat.getId());
			this.documentCurent.setAngajat(angajatComplet);
		}


		// ------------- ZONA 2 -- tip operatiune --------------

		// adaugam si lista de tipuri de operatiuni:
		private List<String> operatiuni;

		public List<String> getOperatiuni() {
			if (operatiuni == null)// primul acces - userul deschide formul
			{
				operatiuni = new LinkedList<String>();
				// Folosim constante declarate specific pentru aceasta situatie, din
				// moment ce nu avem o alta sursa
				// de date.
				// Evident, asta inseamna ca lista va fi "inchisa", orice modificare
				// aici necesitand recompilarea si redistribuirea aplicatiei.
				// De regula, se folosesc fisiere de configurare sau tabele-utilitar
				// in BD.
				// Aici insa, folosim aceasta eroare de design cu titlu de exemplu
				operatiuni.add(RETURNARE_CU_FACTURA);
				operatiuni.add(RETURNARE_CU_BON_FISCAL);
				
			}
			return operatiuni;
		}

		// operatia selectata - o asociem cu tipDocument
		private String operatieSelectata;

		public String getOperatieSelectata() {
			return this.operatieSelectata;
		}

		public void setOperatieSelectata(String operatieSelectata) {
			this.operatieSelectata = operatieSelectata;
		}

		// ------------- ZONA 3 -- date despre documentul curent afisat pe ecran
		// --------------
		// date privind documentul curent = obiectul tinta al formularului si este
		// deja reprezentat (vezi zona 0, atributul documentCurent)

		// ------------- ZONA 4 -- linii document - read only
		// ---------------------------------------------
		// --tabelul afisa totalul receptionat pentru docummentul curent
		// --pentru ca pot fi mai multe receptii pentru acelasi document, este
		// necesara cumularea liniilor fiecarei returnari intr-o singura colectie

		public List<LinieReturnare> getArticoleReturnate() {
			return this.documentCurent.getArticoleReturnare();
		}

		// ------------- ZONA 5 - date despre mijlocul de transaport -
		// -------------------------
		// --nimic de adaugat - se lucreaza cu atributele obiectului documentCurent

		// ------------- ZONA 6 - totaluri
		// ----------------------------------------------------
		// -- nimic de adaugat - se lucreaza cu atributele specifice ale obiectului
		// documentCurent

		// ----------Zona 10 - tabelul receptii ------------------
		// Formularul va prezenta lista de obiecte Receptie reprezentate de colectia
		// OneToMany corespunzatoare a documentCurent
		// trebuie adaugat un atribut ce va tine referinta catre obiectul Receptie
		// curent selectat

		private Returnare ReturnareSelectata;

		public Returnare getReturnareSelectata() {
			return ReturnareSelectata;
		}

		public void setReturnareSelectata(Returnare ReturnareSelectata) {
			this.ReturnareSelectata = ReturnareSelectata;
		}

		// trebuie adaugat si un atribut pentru sursa de date a listei de gestiuni

		private List<Returnare> listaArticoleReturnare;

		public List<Returnare> getListaArticoleReturnare() {
			if (listaArticoleReturnare == null)
				listaArticoleReturnare = masterRepo.findReturnareAll();

			return listaArticoleReturnare;
		}

		// ----------Zona 11 - tabelul de materiale receptionate pe receptia curenta
		// ------------------
		// -- nimic de adaugat aici pentru sursa de date a tabelului- formularul va
		// prezenta colectia liniiDocument a receptiei selectate
		// --de adaugat doar suportul pentru lista de materiale

		private List<Vanzare> listaArticoleVandute;

		public List<Vanzare> getArticoleVandute() {
			if (listaArticoleVandute == null)
				listaArticoleVandute = masterRepo.findVanzareAll();

			return listaArticoleVandute;
		}

		// GETTERI SI SETTERI pentru proprietatile zonei 0

		// metode de acces pentru zona 0
		public DocInsotitor getDocumentCurent() {
			return documentCurent;
		}

		public void setDocumentCurent(DocInsotitor documentCurent) {
			this.documentCurent = documentCurent;
		}

		public List<DocInsotitor> getListaDocumente() {
			return listaDocumente;
		}

		public void setListaDocumente(List<DocInsotitor> listaDocumente) {
			this.listaDocumente = listaDocumente;
		}

		public MasterRepository getMasterRepo() {
			return masterRepo;
		}

		public void setMasterRepo(MasterRepository masterRepo) {
			this.masterRepo = masterRepo;
		}

		public DocumentRepository getDocRepo() {
			return docRepo;
		}

		public void setDocRepo(DocumentRepository docRepo) {
			this.docRepo = docRepo;
		}

		public Object getListaReturnare() {
			// TODO Auto-generated method stub
			return null;
		}

		public Object getListaProduse() {
			// TODO Auto-generated method stub
			return null;
		}

		public List<LinieReturnare> getArticoleReturnate1() {
			// TODO Auto-generated method stub
			return null;
		}


	}




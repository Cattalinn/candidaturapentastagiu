package Stocuri.model.entities;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import Stocuri.metamodel.AbstractEntity;
@Entity
public class LinieInventariere extends AbstractEntity{

	
	Double cantitate=0.0; //valoarea null trebuie evitata cu orice pret aici
	Double pret=0.0; //valoarea null trebuie evitata cu orice pret aici
	@ManyToOne
	Inventariere inventariere;
	@ManyToOne
	Produs produs;
	
	/**
	 * Default constructor mandatory for JPA 
	 */
	public LinieInventariere(){super();}
	
	/**
	 * convenient constructor
	 * @param id
	 * @param cantitate
	 * @param pret
	 *
	 */
	public LinieInventariere(Double cantitate, Double pret,
			Produs produs) {
		this();
		this.cantitate = cantitate;
		this.pret = pret;
		
	}
	
	public Produs getProdus() {
		return produs;
	}

	public void setProdus(Produs produs) {
		this.produs = produs;
	}

	public Double getCantitate() {
		return cantitate;
	}
	public void setCantitate(Double cantitate) {
		this.cantitate = cantitate;
	}
	public Double getPret() {
		return pret;
	}
	public void setPret(Double pret) {
		this.pret = pret;
	}

	public Inventariere getInventariere() {
		return inventariere;
	}

	public void setInventariere(Inventariere inventariere) {
		this.inventariere = inventariere;
	}
}
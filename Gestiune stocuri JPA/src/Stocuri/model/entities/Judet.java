package Stocuri.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import Stocuri.metamodel.AbstractEntity;
import Stocuri.metamodel.AbstractRepository;
@Entity
public class Judet extends AbstractEntity{
@Column(unique=true)
Integer codJudet;

@Column(unique=true)
String numeJudet;

@Column(unique=true)
String regiune;

public Integer getCodJudet() {
	return codJudet;
}

public void setCodJudet(Integer codJudet) {
	this.codJudet = codJudet;
}

public String getNumeJudet() {
	return numeJudet;
}

public void setNumeJudet(String numeJudet) {
	this.numeJudet = numeJudet;
}

public String getRegiune() {
	return regiune;
}

public void setRegiune(String regiune) {
	this.regiune = regiune;
}
}

package Stocuri.model.entities;

import Stocuri.metamodel.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class LinieNecesar extends AbstractEntity{

	
	Double cantitate=0.0; //valoarea null trebuie evitata cu orice pret aici
	Double pret=0.0; //valoarea null trebuie evitata cu orice pret aici
	@ManyToOne
	Necesar necesar;
	@ManyToOne
	Produs produs;
	
	/**
	 * Default constructor mandatory for JPA 
	 */
	public LinieNecesar(){super();}
	
	/**
	 * convenient constructor
	 * @param id
	 * @param cantitate
	 * @param pret
	 *
	 */
	public LinieNecesar(Double cantitate, Double pret,
			Produs produs) {
		this();
		this.cantitate = cantitate;
		this.pret = pret;
		
	}
	
	public Produs getProdus() {
		return produs;
	}

	public void setProdus(Produs produs) {
		this.produs = produs;
	}

	public Double getCantitate() {
		return cantitate;
	}
	public void setCantitate(Double cantitate) {
		this.cantitate = cantitate;
	}
	public Double getPret() {
		return pret;
	}
	public void setPret(Double pret) {
		this.pret = pret;
	}

	public Necesar getNecesar() {
		return necesar;
	}

	public void setNecesar(Necesar necesar) {
		this.necesar = necesar;
	}	
}

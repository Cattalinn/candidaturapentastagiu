package Stocuri.model.entities;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import Stocuri.metamodel.AbstractEntity;
@Entity
public class Inventariere extends AbstractEntity {
	
	private Integer nrInventariere;
	@Temporal(value = TemporalType.DATE)
	private Date dataInventariere;
	
	@OneToMany(mappedBy = "inventariere", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<LinieInventariere> liniiInventariere = new HashSet<LinieInventariere>();

	// ---------- metode pentru managementul colectiei liniiDocument
	// -----------//

	public void addLinieInventariere(LinieInventariere linie) {
		this.liniiInventariere.add(linie);
		linie.setInventariere(this);// set the inverse relation. MANDATORY!
	}

	public void addLinieInventariere(Inventariere Inventariere, Produs produs, Double cantitate,
			Double pret) {
		LinieInventariere linie = new LinieInventariere(cantitate, pret, produs);
		this.liniiInventariere.add(linie);
		linie.setInventariere(this);// set the inverse relation. MANDATORY!
	}

	public void removeLinieInventariere(LinieInventariere linie) {
		this.liniiInventariere.remove(linie);
		linie.setInventariere(null);// detach the inverse relation. MANDATORY!
	}



	/**
	 * Lista returnata este read-only. Utilizati metodele publicate pentru
	 * managementul colectiei.
	 * 
	 * @return - lista read-only
	 */
	public List<LinieInventariere> getLiniiInventariere() {
		return Collections.unmodifiableList(new LinkedList(liniiInventariere));
	}

	
	public Integer getNrInventariere() {
		return nrInventariere;
	}
	public void setNrInventariere(Integer nrInventariere) {
		this.nrInventariere = nrInventariere;
	}
	public Date getDataInventariere() {
		return dataInventariere;
	}
	public void setDataInventariere(Date dataInventariere) {
		this.dataInventariere = dataInventariere;
	}
	
}

package Stocuri.model.entities;

import javax.persistence.Entity;

import Stocuri.metamodel.AbstractEntity;
@Entity
public class Necesar extends AbstractEntity{
	private Integer codNecesar;

	public Integer getCodNecesar() {
		return codNecesar;
	}

	public void setCodNecesar(Integer codNecesar) {
		this.codNecesar = codNecesar;
	}
	
}

package Stocuri.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.UniqueConstraint;

import Stocuri.metamodel.AbstractEntity;

@Entity
public class Localitate extends Judet{
	@Column(unique=true)
	Integer cod;
	
	@Column(unique=true)
	String denumire;
	@ManyToOne 
	private Judet judet;

	public Localitate(){
		
	}
	
	public Judet getJudet() {
		return judet;
	}

	public Localitate(Integer cod, String denumire, Judet judet) {
		super();
		this.cod = cod;
		this.denumire = denumire;
		this.judet = judet;
	}

	public void setJudet(Judet judet) {
		this.judet = judet;
	}

	public String getDenumire() {
		return denumire;
	}

	public void setDenumire(String denumire) {
		this.denumire = denumire;
	}

	public Integer getCod() {
		return cod;
	}

	public void setCod(Integer cod) {
		this.cod = cod;
	}
	
	
}

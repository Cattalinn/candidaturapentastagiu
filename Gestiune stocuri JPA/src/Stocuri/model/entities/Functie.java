package Stocuri.model.entities;

import javax.persistence.Entity;

import Stocuri.metamodel.AbstractEntity;

@Entity
public class Functie extends AbstractEntity{
	private Integer codFunctie;
	private String tipFunctie;
	public Integer getCodFunctie() {
		return codFunctie;
	}
	public void setCodFunctie(Integer codFunctie) {
		this.codFunctie = codFunctie;
	}
	public String getTipFunctie() {
		return tipFunctie;
	}
	public void setTipFunctie(String tipFunctie) {
		this.tipFunctie = tipFunctie;
	}
	
}

package Stocuri.model.entities;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import Stocuri.metamodel.AbstractEntity;
@Entity
public class Returnare extends AbstractEntity{
	private Integer codReturnare;
	
	@Temporal(value = TemporalType.DATE)
	private Date dataFactura;
	
	private String nrDataRefuz;
	
	@Temporal(value = TemporalType.DATE)
	private Date dataRefuz;
	
	private String motivatie;
	
	@OneToMany(mappedBy = "returnare", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<LinieReturnare> liniiReturnare = new HashSet<LinieReturnare>();

	// ---------- metode pentru managementul colectiei liniiDocument
	// -----------//

	public void addLinieReturnare(LinieReturnare linie) {
		this.liniiReturnare.add(linie);
		linie.setReturnare(this);// set the inverse relation. MANDATORY!
	}

	public void addLinieDocument(Returnare Returnare, Produs produs, Double cantitate,
			Double pret) {
		LinieReturnare linie = new LinieReturnare(cantitate, pret, produs);
		this.liniiReturnare.add(linie);
		linie.setReturnare(this);// set the inverse relation. MANDATORY!
	}

	public void removeLinieDocument(LinieReturnare linie) {
		this.liniiReturnare.remove(linie);
		linie.setReturnare(null);// detach the inverse relation. MANDATORY!
	}

	
	
	/**
	 * Lista returnata este read-only. Utilizati metodele publicate pentru
	 * managementul colectiei.
	 * 
	 * @return - lista read-only
	 */
	public List<LinieReturnare> getLiniiReturnare() {
		return Collections.unmodifiableList(new LinkedList(liniiReturnare));
	}

	
	public Integer getCodReturnare() {
		return codReturnare;
	}
	public void setCodReturnare(Integer codReturnare) {
		this.codReturnare = codReturnare;
	}
	public Date getDataFactura() {
		return dataFactura;
	}
	public void setDataFactura(Date dataFactura) {
		this.dataFactura = dataFactura;
	}
	public String getNrDataRefuz() {
		return nrDataRefuz;
	}
	public void setNrDataRefuz(String nrDataRefuz) {
		this.nrDataRefuz = nrDataRefuz;
	}
	public Date getDataRefuz() {
		return dataRefuz;
	}
	public void setDataRefuz(Date dataRefuz) {
		this.dataRefuz = dataRefuz;
	}
	public String getMotivatie() {
		return motivatie;
	}
	public void setMotivatie(String motivatie) {
		this.motivatie = motivatie;
	}

	public void setReturnare(Object listaReturnare) {
		// TODO Auto-generated method stub
		
	}
	

}

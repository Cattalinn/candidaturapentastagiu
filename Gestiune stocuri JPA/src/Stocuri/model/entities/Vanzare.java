package Stocuri.model.entities;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import Stocuri.metamodel.AbstractEntity;
@Entity
public class Vanzare extends AbstractEntity{
private Integer codVanzare;
private String tipVanzare;
@Temporal(value = TemporalType.DATE)
private Date dataVanzare;

@OneToMany(mappedBy = "vanzare", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
private Set<LinieVanzare> liniiVanzare = new HashSet<LinieVanzare>();

// ---------- metode pentru managementul colectiei liniiDocument
// -----------//

public void addLinieVanzare(LinieVanzare linie) {
	this.liniiVanzare.add(linie);
	linie.setVanzare(this);// set the inverse relation. MANDATORY!
}

public void addLinieVanzare(Vanzare vanzare, Produs produs, Double cantitate,
		Double pret) {
	LinieVanzare linie = new LinieVanzare(cantitate, pret, produs);
	this.liniiVanzare.add(linie);
	linie.setVanzare(this);// set the inverse relation. MANDATORY!
}

public void removeLinieVanzare(LinieVanzare linie) {
	this.liniiVanzare.remove(linie);
	linie.setVanzare(null);// detach the inverse relation. MANDATORY!
}



/**
 * Lista returnata este read-only. Utilizati metodele publicate pentru
 * managementul colectiei.
 * 
 * @return - lista read-only
 */
public List<LinieVanzare> getLiniiVanzare() {
	return Collections.unmodifiableList(new LinkedList(liniiVanzare));
}


public Integer getCodVanzare() {
	return codVanzare;
}
public void setCodVanzare(Integer codVanzare) {
	this.codVanzare = codVanzare;
}
public String getTipVanzare() {
	return tipVanzare;
}
public void setTipVanzare(String tipVanzare) {
	this.tipVanzare = tipVanzare;
}
public Date getDataVanzare() {
	return dataVanzare;
}
public void setDataVanzare(Date dataVanzare) {
	this.dataVanzare = dataVanzare;
}

}

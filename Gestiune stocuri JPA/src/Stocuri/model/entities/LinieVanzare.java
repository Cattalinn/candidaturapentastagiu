package Stocuri.model.entities;


import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import Stocuri.metamodel.AbstractEntity;
@Entity
public class LinieVanzare extends AbstractEntity{

	
	Double cantitate=0.0; //valoarea null trebuie evitata cu orice pret aici
	Double pret=0.0; //valoarea null trebuie evitata cu orice pret aici
	@ManyToOne
	Vanzare vanzare;
	@ManyToOne
	Produs produs;
	public Vanzare getVanzare() {
		return vanzare;
	}

	public void setVanzare(Vanzare vanzare) {
		this.vanzare = vanzare;
	}
	
	
	/**
	 * Default constructor mandatory for JPA 
	 */
	public LinieVanzare(){super();}
	
	/**
	 * convenient constructor
	 * @param id
	 * @param cantitate
	 * @param pret
	 *
	 */
	public LinieVanzare(Double cantitate, Double pret,
			Produs produs) {
		this();
		this.cantitate = cantitate;
		this.pret = pret;
		
	}
	
	public Produs getProdus() {
		return produs;
	}

	public void setProdus(Produs produs) {
		this.produs = produs;
	}

	public Double getCantitate() {
		return cantitate;
	}
	public void setCantitate(Double cantitate) {
		this.cantitate = cantitate;
	}
	public Double getPret() {
		return pret;
	}
	public void setPret(Double pret) {
		this.pret = pret;
	}


}
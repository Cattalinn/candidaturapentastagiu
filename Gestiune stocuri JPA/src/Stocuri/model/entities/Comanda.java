package Stocuri.model.entities;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import Stocuri.metamodel.AbstractEntity;

@Entity
public class Comanda extends AbstractEntity{

	String numarComanda;
	@OneToMany(mappedBy = "comanda", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<LinieComanda> liniiComanda = new HashSet<LinieComanda>();

	// ---------- metode pentru managementul colectiei liniiDocument
	// -----------//

	public void addLinieComanda(LinieComanda linie) {
		this.liniiComanda.add(linie);
		linie.setComanda(this);// set the inverse relation. MANDATORY!
	}

	public void addLinieComanda(Comanda comanda, Produs produs, Double cantitate,
			Double pret) {
		LinieComanda linie = new LinieComanda(cantitate, pret, produs);
		this.liniiComanda.add(linie);
		linie.setComanda(this);// set the inverse relation. MANDATORY!
	}

	public void removeLinieComanda(LinieComanda linie) {
		this.liniiComanda.remove(linie);
		linie.setComanda(null);// detach the inverse relation. MANDATORY!
	}

	
	
	/**
	 * Lista returnata este read-only. Utilizati metodele publicate pentru
	 * managementul colectiei.
	 * 
	 * @return - lista read-only
	 */
	public List<LinieComanda> getLiniiComanda() {
		return Collections.unmodifiableList(new LinkedList(liniiComanda));
	}

	
	public String getNumarComanda() {
		return numarComanda;
	}
	public void setNumarComanda(String numarComanda) {
		this.numarComanda = numarComanda;
	}

	

	
}

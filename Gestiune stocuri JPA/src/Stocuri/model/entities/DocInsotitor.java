package Stocuri.model.entities;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class DocInsotitor extends Document {
	private String mijlTransport;

	@ManyToOne
	private DocInsotitor docInsotitorStornat;

	@ManyToOne
	private DocInsotitor docInsotitorDeStornare;

	@ManyToOne 
	private Partener partener;

	@OneToMany(mappedBy = "docInsotitor", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Receptie> receptii = new HashSet<Receptie>();

	// ---------------metode pentru managementul colectiei receptii-----//

	public void addReceptie(Receptie receptie) {
		this.receptii.add(receptie);
		receptie.setDocInsotitor(this);
	}

	public void addReceptie(Gestiune gestiune) {
		Receptie r = new Receptie();
		r.setGestiune(gestiune);
		this.addReceptie(r);
	}

	public void removeReceptie(Receptie receptie) {

		this.receptii.remove(receptie);
		receptie.setDocInsotitor(null);
	}

	public List<Receptie> getReceptii() {
		return Collections.unmodifiableList(new LinkedList(this.receptii));
	}

	/**
	 * Furnizeaza o lista cumulativa cu toate materialele receptionate
	 * 
	 * @return - lista materialelor receptionate - READ ONLY
	 */
/**	public List<LinieComanda> getArticoleReceptionate() {
		List<LinieComanda> artReceptionate = new LinkedList<LinieComanda>();
		for (Receptie receptie : receptii)
			for (LinieComanda row : receptie.getLinieComanda())
				artReceptionate.add(row);
		// lista trebuie sa fie read-only --> nu poate fi  pentru a
		// adauga linii noiutilizata
		return Collections.unmodifiableList(artReceptionate);
	}
*/
	public String getMijlTransport() {
		return mijlTransport;
	}

	public void setMijlTransport(String mijlTransport) {
		this.mijlTransport = mijlTransport;
	}

	public DocInsotitor getDocInsotitorStornat() {
		return docInsotitorStornat;
	}

	public void setDocInsotitorStornat(DocInsotitor docInsotitorStornat) {
		this.docInsotitorStornat = docInsotitorStornat;
	}

	public DocInsotitor getDocInsotitorDeStornare() {
		return docInsotitorDeStornare;
	}

	public void setDocInsotitorDeStornare(DocInsotitor docInsotitorDeStornare) {
		this.docInsotitorDeStornare = docInsotitorDeStornare;
	}

	public Partener getPartener() {
		return partener;
	}

	public void setPartener(Partener partener) {
		this.partener = partener;
	}

	

	public List<LinieReturnare> getArticoleReturnare() {
		// TODO Auto-generated method stub
		return null;
	}

	public void addReturnare(Returnare r) {
		// TODO Auto-generated method stub
		
	}

	public void setAngajat(Angajat angajatComplet) {
		// TODO Auto-generated method stub
		
	}



}

package Stocuri.model.entities;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import Stocuri.metamodel.AbstractEntity;
@Entity
public class Angajat extends AbstractEntity{
private Integer codAngajat;
private String numeAngajat;
private String tipAngajat;

@ManyToOne 
private Departament departament;

@ManyToOne 
private Functie functie;

public Integer getCodAngajat() {
	return codAngajat;
}

public void setCodAngajat(Integer codAngajat) {
	this.codAngajat = codAngajat;
}

public String getNumeAngajat() {
	return numeAngajat;
}

public void setNumeAngajat(String numeAngajat) {
	this.numeAngajat = numeAngajat;
}

public String getTipAngajat() {
	return tipAngajat;
}

public void setTipAngajat(String tipAngajat) {
	this.tipAngajat = tipAngajat;
}

public Departament getDepartament() {
	return departament;
}

public void setDepartament(Departament departament) {
	this.departament = departament;
}

public Functie getFunctie() {
	return functie;
}

public void setFunctie(Functie functie) {
	this.functie = functie;
}


}

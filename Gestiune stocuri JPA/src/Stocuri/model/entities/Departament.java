package Stocuri.model.entities;

import javax.persistence.Entity;

import Stocuri.metamodel.AbstractEntity;

@Entity
public class Departament extends AbstractEntity{
	private Integer codDepartament;
	private Integer tipDepartament;
	
	public Integer getCodDepartament() {
		return codDepartament;
	}
	public void setCodDepartament(Integer codDepartament) {
		this.codDepartament = codDepartament;
	}
	public Integer getTipDepartament() {
		return tipDepartament;
	}
	public void setTipDepartament(Integer tipDepartament) {
		this.tipDepartament = tipDepartament;
	}
	
	
}

package Stocuri.model.entities;

import Stocuri.metamodel.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class LinieReturnare extends AbstractEntity{

	
	Double cantitate=0.0; //valoarea null trebuie evitata cu orice pret aici
	Double pret=0.0; //valoarea null trebuie evitata cu orice pret aici
	@ManyToOne
	Returnare returnare;
	@ManyToOne
	Produs produs;
	
	/**
	 * Default constructor mandatory for JPA 
	 */
	public LinieReturnare(){super();}
	
	/**
	 * convenient constructor
	 * @param id
	 * @param cantitate
	 * @param pret
	 *
	 */
	public LinieReturnare(Double cantitate, Double pret,
			Produs produs) {
		this();
		this.cantitate = cantitate;
		this.pret = pret;
		
	}
	
	public Produs getProdus() {
		return produs;
	}

	public void setProdus(Produs produs) {
		this.produs = produs;
	}

	public Double getCantitate() {
		return cantitate;
	}
	public void setCantitate(Double cantitate) {
		this.cantitate = cantitate;
	}
	public Double getPret() {
		return pret;
	}
	public void setPret(Double pret) {
		this.pret = pret;
	}

	public Returnare getReturnare() {
		return returnare;
	}

	public void setReturnare(Returnare returnare) {
		this.returnare = returnare;
	}

	public void setProduseReturnate(LinieReturnare linieReturnare) {
		// TODO Auto-generated method stub
		
	}

	
}

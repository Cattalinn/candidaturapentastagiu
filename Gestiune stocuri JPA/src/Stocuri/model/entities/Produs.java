package Stocuri.model.entities;

import javax.persistence.Entity;

import Stocuri.metamodel.AbstractEntity;
@Entity
public class Produs extends AbstractEntity {
	
private Integer codProdus;
private String denumireProdus;
private String um;


public Integer getCodProdus() {
	return codProdus;
}
public void setCodProdus(Integer codProdus) {
	this.codProdus = codProdus;
}
public String getDenumireProdus() {
	return denumireProdus;
}
public void setDenumireProdus(String denumireProdus) {
	this.denumireProdus = denumireProdus;
}
public String getUm() {
	return um;
}
public void setUm(String um) {
	this.um = um;
}

}

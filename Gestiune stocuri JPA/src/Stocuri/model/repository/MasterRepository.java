package Stocuri.model.repository;

import java.util.List;

import Stocuri.metamodel.TransactionManagement;
import Stocuri.model.entities.Angajat;
import Stocuri.model.entities.Produs;
import Stocuri.model.entities.Partener;
import Stocuri.model.entities.Returnare;
import Stocuri.model.entities.Vanzare;

/**
 * Defineste operatii CRUD pentru majoritatea claselor-satelit din modelul nostru.
 * Exemple: Furnizor, Vanzare, Produs, Returnare
 * @author cretuli
 *
 */
public interface MasterRepository extends TransactionManagement{
	
	//metodele de tip Create trebuie sa returneze clientului noua versiune a obiectului dupa insertul in BD 
	public Vanzare addVanzare(Vanzare vanzare);
	public Partener addPartener(Partener partener);
	public Produs addProdus(Produs produs);
	public Returnare addReturnare(Returnare returnare);
	public Angajat addAngajat(Angajat angajat);

	
	//metodele de tip Update trebuie sa returneze clientului noua versiune a obiectului dupa update-ul in BD
	public Vanzare updateVanzare(Vanzare vanzare);
	public Partener updatePartener(Partener partener);
	public Produs updateProdus(Produs produs);
	public Returnare updateReturnare(Returnare returnare);
	public Angajat updateAngajat(Angajat angajat);

	
	public void deleteVanzare(Vanzare vanzare);
	public void deleteFurnizor(Partener partener);
	public void deleteProdus(Produs produs);
	public void deleteReturnare(Returnare returnare);
	public void deleteAngajat(Angajat angajat);
	
	public List<Vanzare> findVanzareAll();
	public List<Partener> findPartenerAll();
	public List<Produs> findProdusAll();
	public List<Returnare> findReturnareAll();
	public List<Angajat> findAngajatAll();
	
	
	public Vanzare findVanzareById(Long id);
	public Partener findPartenerById(Long id);
	public Produs findProduseById(Long id);
	public Returnare findReturnareById(Long id);
	public Angajat findAngajatByID(Long id);
	
	/**
	 * Extrage date doar din tabela Furnizor, fara relatia ManyToOne Vanzare
	 * @return
	 */
	public List<Partener> findParteneriAllLight();
	public List<Angajat> findAngajatAllLight();



}

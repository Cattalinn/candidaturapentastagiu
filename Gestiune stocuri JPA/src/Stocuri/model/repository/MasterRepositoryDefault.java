package Stocuri.model.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import Stocuri.metamodel.AbstractRepository;
import Stocuri.model.entities.Angajat;
import Stocuri.model.entities.Vanzare;
import Stocuri.model.entities.Partener;
import Stocuri.model.entities.Returnare;
import Stocuri.model.entities.Produs;

public class MasterRepositoryDefault extends AbstractRepository implements
		MasterRepository {

	@Override
	public Vanzare addVanzare(Vanzare vanzare) {
		return (Vanzare)this.create(vanzare);

	}

	@Override
	public Partener addPartener(Partener partener) {
		return (Partener)this.create(partener);

	}

	@Override
	public Returnare addReturnare(Returnare returnare) {
		return (Returnare)this.create(returnare);

	}

	@Override
	public Produs addProdus(Produs produs) {
		return (Produs)this.create(produs);
	}
	

	@Override
	public void deleteVanzare(Vanzare vanzare) {
		this.delete(vanzare);

	}

	@Override
	public void deleteFurnizor(Partener partener) {
		this.delete(partener);

	}

	@Override
	public void deleteReturnare(Returnare returnare) {
		this.delete(returnare);

	}

	@Override
	public void deleteProdus(Produs produs) {
		this.delete(produs);

	}

	@Override
	public Vanzare findVanzareById(Long id) {
		// TODO Auto-generated method stub
		return this.findVanzareById(id);
	}

	@Override
	public List<Vanzare> findVanzareAll() {
		return this.getEm().createQuery("Select v from Vanzare v")
				.getResultList();
	}

	@Override
	public Partener findPartenerById(Long id) {

		return (Partener) this.getEm().createQuery(
				"Select p from Partener f where id=:id").setParameter("id", id)
				.getSingleResult();
	}

	@Override
	public List<Partener> findPartenerAll() {

		return this.getEm().createQuery("Select p from Partener p")
				.getResultList();
	}

	@Override
	public List<Partener> findParteneriAllLight() {
		// maxima atentie la ordinea parametrilor
		return this
				.getEm()
				.createQuery(
						"Select new Partener( p.codPartener, p.numePartener,p.tipPartener, p.adresa, p.banca, p.contBancar, p.sold) from Partener p")
				.getResultList();
	}

	@Override
	public Returnare findReturnareById(Long id) {
		// TODO Auto-generated method stub
		return this.findReturnareById(id);
	}

	@Override
	public List<Returnare> findReturnareAll() {
		return this.getEm().createQuery("Select r from Returnare r")
				.getResultList();
	}

	@Override
	public Produs findProduseById(Long id) {
		// TODO Auto-generated method stub
		return this.findProduseById(id);
	}

	@Override
	public List<Produs> findProdusAll() {

		return this.getEm().createQuery("Select l from Produs l")
				.getResultList();
	}

	@Override
	public Vanzare updateVanzare(Vanzare Vanzare) {
		// TODO Auto-generated method stub
		return this.updateVanzare(Vanzare);
	}

	@Override
	public Partener updatePartener(Partener partener) {
		// TODO Auto-generated method stub
		return this.updatePartener(partener);
	}

	@Override
	public Returnare updateReturnare(Returnare returnare) {
		// TODO Auto-generated method stub
		return this.updateReturnare(returnare);
	}

	@Override
	public Produs updateProdus(Produs produs) {
		// TODO Auto-generated method stub
		return this.updateProdus(produs);
	}

	@Override
	public Angajat addAngajat(Angajat angajat) {
		// TODO Auto-generated method stub
		return this.addAngajat(angajat);
	}

	@Override
	public Angajat updateAngajat(Angajat angajat) {
		// TODO Auto-generated method stub
		return this.updateAngajat(angajat);
	}

	@Override
	public void deleteAngajat(Angajat angajat) {
		// TODO Auto-generated method stub
		this.deleteAngajat(angajat);
	}

	@Override
	public List<Angajat> findAngajatAll() {
		// TODO Auto-generated method stub
		return this.findAngajatAll();
	}

	@Override
	public Angajat findAngajatByID(Long id) {
		// TODO Auto-generated method stub
		return this.findAngajatByID(id);
	}

	@Override
	public List<Angajat> findAngajatAllLight() {
		// TODO Auto-generated method stub
		return this.findAngajatAllLight();
	}

	

}

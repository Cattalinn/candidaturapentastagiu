package Stocuri.metamodel;

public interface TransactionManagement {
	public void beginTransaction();
	public void commitTransaction();
}

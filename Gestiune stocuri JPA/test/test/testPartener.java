package test;


import java.util.Iterator;
import java.util.List;

import junit.framework.Assert;



import Stocuri.model.entities.Partener;

import Stocuri.model.repository.MasterRepository;
import Stocuri.model.repository.MasterRepositoryDefault;

/**
 * Un test prin care verificam cele doua metode de incarecare a datelor despre
 * Parteneri:
 * 
 * # {@link MasterRepository#findParteneriAll()} - incarca complet graful de
 * obiecte, inclusiv obiecte Localitate corespunzatoare relatiilor ManyToMany
 * 
 * # {@link MasterRepository#findParteneriAllLight()} - incarca doar datele
 * specificate prin constructorul Partener care ignora atributul
 * <code> localitate </code>
 * 
 * 
 * @author cretuli
 * 
 */
public class testPartener {

	static MasterRepository repo = new MasterRepositoryDefault();

	public static void main(String[] args) {
		List<Partener> x = repo.findPartenerAll();
		if (x.size() == 0)// daca nu sunt date in tabela, adaugam date de test
		{
			adaugaParteneri();
			x = repo.findPartenerAll();
		}

		Assert.assertTrue( x.size() > 0);
	}

	public static void adaugaParteneri() {
		
		Partener g = null;
		repo.beginTransaction();

		for (int i=0; i<7; i++) {
			
			g=new Partener();
			g.setId(g.getId());
			g.setNume("Partener "+g.getId());
			g.setCUI(g.getCUI());
			g.setContBancar(g.getContBancar());
			g.setAdresa(g.getAdresa());
			g.setBanca(g.getBanca());
			g.setSold(g.getSold());
			
			repo.addPartener(g);
		}
		repo.commitTransaction();
	}
}
